# Rpi-Geth#

This is the Dockerfile of [Rpi-Geth Docker Image](https://hub.docker.com/r/cmaiorano/rpi-geth/), this image will run only on ARM devices. It uses (for now) a base image derived from ARM v7 Raspbian Image.