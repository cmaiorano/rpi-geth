FROM resin/rpi-raspbian:jessie

ENV ARM_VERSION arm7
ENV GETH_VERSION  1.5.9
ENV COMMIT_HASH a07539fb

RUN apt-get update && apt-get install -y wget curl  && \
    wget https://gethstore.blob.core.windows.net/builds/geth-linux-$ARM_VERSION-$GETH_VERSION-$COMMIT_HASH.tar.gz && \
    tar xzf geth-linux-$ARM_VERSION-$GETH_VERSION-$COMMIT_HASH.tar.gz && \
    mv geth-linux-$ARM_VERSION-$GETH_VERSION-$COMMIT_HASH/geth /usr/local/bin

ENTRYPOINT ["geth"]

